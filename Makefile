PROG   := utrigger
PREFIX ?= /usr/local
CFLAGS += -Wall -Wextra -Werror

BINDIR=$(DESTDIR)$(PREFIX)/bin
MANDIR=$(DESTDIR)$(PREFIX)/share/man/man1

$(PROG): $(PROG).c

install: $(PROG)
	install -DZ $(PROG) -t $(BINDIR)

uninstall:
	rm -f -- $(BINDIR)/$(PROG)

man: $(PROG)
	help2man --no-discard-stderr --no-info -h -h -v -v ./$(PROG) > $(PROG).1

install.man:
	install -DZ $(PROG).1 -t $(MANDIR)

uninstall.man:
	rm -f -- $(MANDIR)/$(PROG).1

clean:
	rm -f -- $(PROG) $(PROG).1
