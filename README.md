# utrigger

Trigger uevents for devices that are currently present.

During boot, devices may be plugged in before userspace is ready to process
the 'add' events.
`utrigger` retriggers events so that uevent listeners can process the
'coldplug' devices.

Furthermore, the tool can be used for testing and debugging uevent listeners.

## Compilation & Installation

Compilation requires `help2man` and is a simple:

    make

To install:

    sudo make install

By default, `utrigger` will be installed to `/usr/local`.
Set the variable `PREFIX` to change this:

    sudo env PREFIX=/usr/local make install

Manpages are generated with `help2man`:

    make man
    sudo make install.man

## Usage

Call the program directly after launching your preferred device manager:

    busybox uevent &
    utrigger

You can also target specific subsystems:

    utrigger -s net

Or trigger remove events rather than add events:

    utrigger -r
    utrigger -r -s acpi

## Notes and Limitations

The subsystem option takes a glob pattern, which is `*` by default.

This tool can only be sure that events are triggered, not processed.
Consult the device manager or hotplug helper documentation for determining the
latter.

## See Also

Device managers:

* [udev](https://www.freedesktop.org/software/systemd/man/udev.html)
* [eudev](https://github.com/gentoo/eudev)
* [busybox-uevent](https://git.busybox.net/busybox/tree/util-linux/uevent.c)
* [nldev](https://core.suckless.org/nldev)
* [vdev (not maintained)](https://git.devuan.org/unsystemd/vdev)

Hotplug helpers:

* [mdev](https://git.busybox.net/busybox/tree/docs/mdev.txt)
* [smdev](https://core.suckless.org/smdev)
