#ifndef MSG_H
#define MSG_H

#include "stdio.h"
#include "string.h"
#include "errno.h"

static inline int msg_errno(const char *str) {
	fprintf(stderr, "%s: %s\n", str, strerror_l(errno, NULL));
	return -errno;
}

static inline int msg_err(const char *str) {
	fprintf(stderr, "%s\n", str);
	return -1;
}

#endif /* MSG_H */
