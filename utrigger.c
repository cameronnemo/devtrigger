/* SPDX-License-Identifier: 0BSD */

#include <stdlib.h>
#include <limits.h>
#include <glob.h>
#include <libgen.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sched.h>

#include "msg.h"
#include "burno.h"

#define UTI_ACTION_DEFAULT "add"
#define UTI_ACTION_REMOVE "remove"

#define UTI_VERSION "0.0.2"
#define UTI_OPT_STR "vhdrs:"

static const char* action;
static int log_priority = 0;

int print_usage(const char *name, int ret);
int print_version(const char *name);
int trigger_sys_safe(const char* subsystem);
int trigger_sys(const char* subsystem);
int trigger_glob(const char* pattern);
int write_uevent(const char* path);

int main(int argc, char **argv) {
	int   opt;
	char *subsystem, *name;

	name = basename(argv[0]);
	action = UTI_ACTION_DEFAULT;
	subsystem = NULL;

	while ((opt = getopt(argc, argv, UTI_OPT_STR)) != -1) {
		switch (opt) {
			case 'v':
				return print_version(name);
			case 'h':
				return print_usage(name, EXIT_SUCCESS);
			case 'd':
				log_priority = 1;
				break;
			case 'r':
				action = UTI_ACTION_REMOVE;
				break;
			case 's':
				subsystem = optarg;
				break;
			case '?':
			default:
				return print_usage(name, 2);
		}
	}

	if (optind < argc) return print_usage(name, 2);

	return trigger_sys_safe(subsystem) == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}

int print_usage(const char *name, int ret) {
	fprintf(stderr, "Usage: %s [OPTIONS]\n\n", name);
	fprintf(stderr,
		"\t-h\t\tshow this help message\n"
	        "\t-v\t\tprint version string\n"
		"\t-d\t\tprint events and paths triggered to stdout\n"
		"\t-r\t\ttrigger a remove event rather than add\n"
		"\t-s SUBSYSTEM\tonly trigger events for the specified subsystem\n\n");
	return ret;
}

int print_version(const char *name) {
	fprintf(stderr, "%s (%s)\n", name, UTI_VERSION);
	return EXIT_SUCCESS;
}

/**
 * trigger_sys_safe:
 * @subsystem: glob pattern to sanitize and pass to trigger_sys()
 *
 * Ensures that @subsystem is a path component or NULL.
 *
 * Returns: 0 on success, -1 on failure
 **/
int trigger_sys_safe(const char* subsystem) {
	if (!subsystem) return trigger_sys(subsystem);

	if (strnlen(subsystem, NAME_MAX + 1) > NAME_MAX)
		return msg_err("subsystem field is too long");
	if (strchr(subsystem, '/') != NULL)
		return msg_err("subsystem field must not contain slashes");

	return trigger_sys(subsystem);
}

/**
 * trigger_sys:
 * @subsystem:	glob pattern to match to subsystem names
 *
 * Triggers events for devices in /sys.
 *
 * Returns: 0 on success, -1 on failure
 **/
int trigger_sys(const char* subsystem) {
	size_t len;

	if (subsystem) len = strlen(subsystem);
	else len = 1;

	/* this is the maximum size of the string, minus the subsystem */
	len += sizeof "/sys/subsystem//devices/*/uevent";

	char pattern[len];
	int ret;

	/* According to kernel docs, we should ignore /sys/block, /sys/class,
	 * and /sys/bus when we have /sys/subsystem. As of 5.0, it is nowhere
	 * to be found.
	 */

	if (access("/sys/subsystem", F_OK) == 0) {
		if (!subsystem)
			return trigger_glob("/sys/subsystem/*/devices/*/uevent");
		ret = snprintf(pattern, len,
		               "/sys/subsystem/%s/devices/*/uevent",
		               subsystem);
		if (ret < 0 || (size_t)ret >= len)
			return msg_err("snprintf");
		return trigger_glob(pattern);
	}

	if (!subsystem) {
		if (trigger_glob("/sys/block/*/uevent") != 0 ||
		    trigger_glob("/sys/class/*/*/uevent") != 0 ||
		    trigger_glob("/sys/bus/*/devices/*/uevent") != 0)
			return -1;
		return 0;
	}

	if (strncmp(subsystem, "block", sizeof "block") == 0
	    && access("/sys/class/block", F_OK) != 0)
		return trigger_glob("/sys/block/*/uevent");

	ret = snprintf(pattern, len, "/sys/class/%s", subsystem);
	if (ret < 0 || (size_t)ret >= len)
		return msg_err("snprintf");

	if (access(pattern, F_OK) == 0) {
		ret = snprintf(pattern, len, "/sys/class/%s/*/uevent",
		               subsystem);
		if (ret < 0 || (size_t)ret >= len)
			return -1;
		return trigger_glob(pattern);
	}

	ret = snprintf(pattern, len, "/sys/bus/%s", subsystem);
	if (ret < 0 || (size_t)ret >= len)
		return msg_err("snprintf");

	if (access(pattern, F_OK) == 0) {
		ret = snprintf(pattern, len, "/sys/bus/%s/devices/*/uevent",
		               subsystem);
		if (ret < 0 || (size_t)ret >= len)
			return msg_err("snprintf");
		return trigger_glob(pattern);
	}

	return -1;
}

/**
 * trigger_glob:
 * @g: glob pattern that matches to desired uevent files
 *
 * Triggers add events for all uevent files matching the pattern.
 *
 * Returns: 0 on success, -1 on error
 **/
int trigger_glob(const char* pattern) {
	__burnglob glob_t uevents;
	int r = glob(pattern, 0, NULL, &uevents);

	if (r == GLOB_NOMATCH) return 0;
	else if (r != 0) return msg_errno("glob");

	for (size_t i = 0; i < uevents.gl_pathc; i += 1) {
		if (write_uevent(uevents.gl_pathv[i]) < 0)
			r = -1;
	}

	return r;
}

/**
 * write_uevent:
 * @path: path of a uevent file to write to
 *
 * Writes to a uevent file in sysfs, triggering an add or remove event.
 *
 * Returns: 1 on successful write, 0 if file not found, -1 on error
 **/
int write_uevent(const char* path) {
	__burnfd int uevent;

	if (log_priority > 0)
		printf("%s %s\n", action, path);

	/* try to open uevent file, if there is one */
	errno = 0;
	uevent = open(path, O_WRONLY);

	if (uevent < 0) {
		/* if the file no longer exists (device went away), return 0 */
		if (errno == ENOENT) return 0;
		return msg_errno("open");
	}

	/* opened the file, so write the event */
	if (dprintf(uevent, "%s", action) < 0) return msg_errno("dprintf");

	sched_yield();
	return 1;
}
